import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'tf-timeline',
    templateUrl: './timeline.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class TimelineComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}