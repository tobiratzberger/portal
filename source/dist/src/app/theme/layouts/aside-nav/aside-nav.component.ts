import { Component, OnInit, ViewEncapsulation, AfterViewInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Helpers } from '../../../helpers';

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class AsideNavComponent implements OnInit, AfterViewInit {

    private url: string = 'https://api.jsonbin.io/b/5a738346d5ad5c24744f08c6';
    public items: any[] = [];

    constructor(private http: HttpClient) {
        http.get(this.url).subscribe(response => {
            this.items = response as any[];
        });
    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        mLayout.initAside();

    }

}