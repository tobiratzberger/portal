import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        'path': '',
        'component': ThemeComponent,
        'children': [
            {
                'path': 'index',
                'loadChildren': '.\/pages\/blank\/blank.module#BlankModule',
            },
            {
                'path': 'profile',
                'loadChildren': '.\/pages\/profile\/profile.module#ProfileModule',
            },
            {
                'path': 'timeline',
                'loadChildren': '.\/pages\/timeline\/timeline.module#TimelineModule',
            },
            {
                'path': '',
                'redirectTo': 'index',
                'pathMatch': 'full',
            },
        ],
    },
    {
        'path': '**',
        'redirectTo': 'index',
        'pathMatch': 'full',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ThemeRoutingModule { }